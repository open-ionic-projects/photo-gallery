# photo-gallery

## Prerequisites to run the project
1.  First, install Node.js. LTS
2.  Then, install the latest Ionic command-line tools in your terminal `npm install -g ionic`

## Steps to run the project
1.  `npm install` -> to get letest dependencies
2.  `ionic serve` -> run the ionic server
3.  Navigate to `http://localhost:8100/`.
